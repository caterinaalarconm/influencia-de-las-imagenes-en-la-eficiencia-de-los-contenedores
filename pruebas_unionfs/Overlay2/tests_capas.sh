#!/bin/bash

capa=$2
echo ""
echo  "### Pruebas escritura en la capa $capa ###"
echo ""
### PRUEBAS ESCRITURA ###

################ Prueba capa más profunda ################

#1MBa

docker exec unionfs_container_$1 dd if=/dev/zero of=/dir_$capa/fich_1MB bs=1048576 count=1 status=progress

#10MB

docker exec unionfs_container_$1 dd if=/dev/zero of=/dir_$capa/fich_10MB bs=10485760 count=1 status=progress

#100MB

docker exec unionfs_container_$1 dd if=/dev/zero of=/dir_$capa/fich_100MB bs=104857600 count=1 status=progress

#1GB

docker exec unionfs_container_$1 dd if=/dev/zero of=/dir_$capa/fich_1GB bs=1073741824 count=1 status=progress


echo ""
echo  "### Pruebas modificar ficheros ###"
echo ""
### PRUEBAS MODIFICAR FICHEROS ###

docker exec unionfs_container_$1 dd if=/dev/zero of=/dir_$capa/fich_1MB bs=1048576 seek=1 count=1 status=progress

docker exec unionfs_container_$1 dd if=/dev/zero of=/dir_$capa/fich_10MB bs=10485760 seek=1 count=1 status=progress

docker exec unionfs_container_$1 dd if=/dev/zero of=/dir_$capa/fich_100MB bs=104857600 seek=1 count=1 status=progress

docker exec unionfs_container_$1 dd if=/dev/zero of=/dir_$capa/fich_1GB bs=1073741824 seek=1 count=1 status=progress

echo ""

#docker exec unionfs_container_4 dd if=/dev/zero of=/dir_0/fich_100MB bs=104857600 seek=1 count=1 status=progress

