#!/bin/bash
echo ""
echo  "### Pruebas escritura en la capa escritura/lectura del container unionfs_container_$1 ###"
echo ""

#1MBa

docker exec unionfs_container_$1 dd if=/dev/zero of=/fich_1MB bs=1048576 count=1 status=progress

#10MB

docker exec unionfs_container_$1 dd if=/dev/zero of=/fich_10MB bs=10485760 count=1 status=progress

#100MB

docker exec unionfs_container_$1 dd if=/dev/zero of=/ich_100MB bs=104857600 count=1 status=progress

#1GB

docker exec unionfs_container_$1 dd if=/dev/zero of=/fich_1GB bs=1073741824 count=1 status=progress
