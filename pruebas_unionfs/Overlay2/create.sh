#!/bin/bash
echo  "exec: (docker build -t image_unionfs_$2 ../../Dockerfiles/df$3)"

docker build -t image_unionfs_$2 ../../Dockerfiles/df$3

docker run -dit --name unionfs_container_$1 image_unionfs_$2 bash
