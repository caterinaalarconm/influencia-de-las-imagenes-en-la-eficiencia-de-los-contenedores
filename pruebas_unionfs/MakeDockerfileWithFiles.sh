#!/bin/bash
layers=$1
dfnum=$2
i=0
echo "FROM ubuntu:latest" > ../Dockerfiles/df$dfnum/Dockerfile
while [ $i -lt $layers ]
do
 echo "RUN touch file_$i" >> ../Dockerfiles/df$dfnum/Dockerfile
 ((i++))
done
