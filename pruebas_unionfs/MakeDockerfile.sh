#!/bin/bash
layers=$1
dfnum=$2
i=0
echo "FROM ubuntu:latest" > ../Dockerfiles/df$dfnum/Dockerfile
while [ $i -lt $layers ]
do
 echo "RUN mkdir dir_$i" >> ../Dockerfiles/df$dfnum/Dockerfile
 ((i++))
done
