#Crear container con el volumen (monta el volumen my-vol en /app/)
#docker run -dit --name bind-container --mount type=bind,source="$(pwd)/hd_bind_mount",target=/app ubuntu-pv bash

#Copiar ficheros

echo ""
echo  "### Pruebas escritura ###"
echo ""

#1MB

docker exec bind-container dd if=/dev/zero of=/app/fich_1MB bs=1048576 count=1 status=progress

#10MB

docker exec bind-container dd if=/dev/zero of=/app/fich_10MB bs=10485760 count=1 status=progress

#100MB

docker exec bind-container dd if=/dev/zero of=/app/fich_100MB bs=104857600 count=1 status=progress

#1GB

docker exec bind-container dd if=/dev/zero of=/app/fich_1GB bs=1073741824 count=1 status=progress

echo ""
echo  "### Pruebas modificar ###"
echo ""

docker exec bind-container dd if=/dev/zero of=/app/fich_1MB bs=1048576 seek=1 count=1 status=progress

docker exec bind-container dd if=/dev/zero of=/app/fich_10MB bs=10485760 seek=1 count=1 status=progress

docker exec bind-container dd if=/dev/zero of=/app/fich_100MB bs=104857600 seek=1 count=1 status=progress

docker exec bind-container dd if=/dev/zero of=/app/fich_1GB bs=1073741824 seek=1 count=1 status=progress
