# Influencia de las imágenes en la eficiencia de los contenedores

### Dockerfile

Se puede crear un nuevo Dockerfile añadiendo una nueva carpeta en el directorio Dockerfiles del estilo `dfX` y ejecutar el script `MakeDockerfile.sh`


### Imagen y container

Haciendo uso del script `create.sh` con los parámetros número de container, número de imagen y número de Dockerfile se crea una nueva imagen y un container a partir de esta.
